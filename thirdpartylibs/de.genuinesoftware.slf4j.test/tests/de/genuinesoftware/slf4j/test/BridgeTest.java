package de.genuinesoftware.slf4j.test;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;

import org.junit.After;
import org.junit.BeforeClass;

public class BridgeTest {

	static class TestLogHandler extends Handler {
	
		List<LogRecord> logRecords = new ArrayList<LogRecord>();
	
		@Override
		public void publish(LogRecord record) {
			logRecords.add(record);
		}
	
		@Override
		public void flush() {
			logRecords.clear();
		}
	
		public List<LogRecord> logRecords() {
			return logRecords;
		}
	
		@Override
		public void close() throws SecurityException {
	
		}
	}

	protected static TestLogHandler logHandler;

	@BeforeClass
	public static void registerJDK14LoggerHandler() {
		logHandler = new TestLogHandler();
		logHandler.setLevel(Level.ALL);
		java.util.logging.Logger globalLogger = LogManager.getLogManager().getLogger("");
		globalLogger.setLevel(Level.ALL);
		Handler[] handlers = globalLogger.getHandlers();
		for(Handler handler : handlers) {
			globalLogger.removeHandler(handler);
		}
		globalLogger.addHandler(logHandler);
		//		globalLogger.addHandler(new ConsoleHandler());
	}

	public BridgeTest() {
		super();
	}

	@After
	public void clearLogHandler() {
		logHandler.flush();
	}
	
	protected List<LogRecord> getLogRecords() {
		return logHandler.logRecords();
	}

}