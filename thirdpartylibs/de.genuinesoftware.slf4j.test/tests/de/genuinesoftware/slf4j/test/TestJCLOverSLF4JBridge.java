package de.genuinesoftware.slf4j.test;

import java.io.IOException;
import java.util.logging.Level;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestJCLOverSLF4JBridge extends BridgeTest {

	private Log logger;
	@Before
	public void createLogger() {
		logger = LogFactory.getLog(TestJCLOverSLF4JBridge.class);
	}

	@Test
	public void test_debug() {
		logger.debug("Debug message");
		Assert.assertEquals(1,getLogRecords().size());
		Assert.assertEquals(Level.FINE,getLogRecords().get(0).getLevel());
		Assert.assertEquals("Debug message",getLogRecords().get(0).getMessage());
	}

	@Test
	public void test_info() throws IOException {
		logger.info("Info message");
		Assert.assertEquals(1,getLogRecords().size());
		Assert.assertEquals(Level.INFO,getLogRecords().get(0).getLevel());
		Assert.assertEquals("Info message",getLogRecords().get(0).getMessage());
	}

	@Test
	public void test_error_expection() {
		Exception t = new Exception("Exception");
		logger.error("Exception occured", t);
		Assert.assertEquals(1,getLogRecords().size());
		Assert.assertEquals(Level.SEVERE,getLogRecords().get(0).getLevel());
		Assert.assertEquals("Exception occured",getLogRecords().get(0).getMessage());
		Assert.assertSame(t,getLogRecords().get(0).getThrown());
	}
}
