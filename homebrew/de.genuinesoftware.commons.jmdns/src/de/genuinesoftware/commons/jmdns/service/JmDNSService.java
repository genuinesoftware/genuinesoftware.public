package de.genuinesoftware.commons.jmdns.service;

import java.io.IOException;

import javax.jmdns.JmDNS;

import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class JmDNSService  {
	
	private static final Logger logger = LoggerFactory
			.getLogger(JmDNSService.class);
	private JmDNS jmdns;
	private ServiceRegistration<JmDNS> serviceRegistration;
	
	synchronized void activate(ComponentContext context)  {
		try {
			jmdns = JmDNS.create(null, null);
			serviceRegistration = context.getBundleContext().registerService(JmDNS.class, jmdns, null);
			logger.info("Started JmDNS {} at address {}.",new Object[] {jmdns.getName(), jmdns.getInterface()});
		} catch (IOException e) {
			logger.error("Error occured while creating JmDNS instance.", e);
		}
	}
	
	synchronized void deactivate() {
		try {
			serviceRegistration.unregister();
			jmdns.close();
			logger.info("Closed JmDNS instance {}", jmdns.getName());
			jmdns = null;
		} catch (IOException e) {
			logger.error("Error occured while closing JmDNS instance.", e);
		}
		this.jmdns = null;
	}
	
}