package de.genuinesoftware.commons.thrift;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;

import org.apache.thrift.TProcessor;
import org.apache.thrift.TServiceClient;
import org.apache.thrift.async.TAsyncClient;
import org.apache.thrift.async.TAsyncClientManager;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.protocol.TProtocolFactory;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.server.TThreadedSelectorServer;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TNonblockingServerSocket;
import org.apache.thrift.transport.TNonblockingSocket;
import org.apache.thrift.transport.TNonblockingTransport;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ThriftUtil {

	public static class ThriftClient<C,T extends TTransport> {

		private C client;
		private T transport;

		public ThriftClient(C client, T transport) {
			this.client = client;
			this.transport = transport;
		}

		public C getClient() {
			return client;
		}

		public void connect() throws TTransportException {
			logger.info("Connecting Thrift client {}...", this);
			if(!(transport instanceof TNonblockingSocket))
				transport.open();
		}

		public void disconnect() {
			if(transport.isOpen())
				transport.close();
		}

		public T getTransport() {
			return transport;
		}

		@Override
		public String toString() {
			return ThriftClient.class.getSimpleName() + " (interface = " + client.getClass().getName() + ", address = " + transport + ")";
		}
	}

	public static class ThriftServer<S extends TServer> {

		private final S server;
		private final InetSocketAddress address;
		private long id;
		private final Class iface;
		private static long counter = 0; 

		public ThriftServer(S server, Class iface, InetSocketAddress address) {
			this.server = server;
			this.address = address;
			this.iface = iface;
			this.id = counter++;
		}

		/**
		 * The service interface of the server.
		 * @return The service interface.
		 */
		public Class getInterface() {
			return iface;
		}

		public S getTServer() {
			return server;
		}

		/**
		 * Starts the server.
		 * <p>
		 * This method does not block. The server is started in a dedicated daemon thread. 
		 * </p>
		 */
		public synchronized void start() {
			if(server.isServing()) {
				logger.warn("{} already started", this);
				return;
			}

			Thread thread = new Thread(new Runnable() {
				public void run() {
					logger.info("Starting {}", ThriftServer.this);
					server.serve();
					logger.info("Stopped {}", ThriftServer.this);
				}
			});
			thread.setDaemon(true);
			thread.setName(ThriftServer.class.getSimpleName() + "_" + id);
			thread.start();
		}

		/**
		 * Stops the server.
		 * <p>
		 * 
		 * </p>
		 */
		public synchronized void stop() {
			server.stop();
		}

		@Override
		public String toString() {
			return ThriftServer.class.getSimpleName() + "_" + id + " (type = " + server.getClass().getSimpleName() +", interface = " + iface.getName() + ", address = " + address.getHostName()  + ":" + address.getPort() + ")";
		}

		public InetSocketAddress getSocketAddress() {
			return address;
		}
	}

	private static final String LOG_MSG__INVALID_INTERFACE = "Given interface is not a Thrift service interface.";

	private static final Logger logger = LoggerFactory.getLogger(ThriftUtil.class);


	/**
	 * Creates a synchronous Thrift Client using a TSocket as transport and the TCompactProtocol for serialization / deserialization.
	 * <p>
	 * The created client cannot be used with non-blocking servers, as it does not utilize {@link TFramedTransport}. 
	 * </p>
	 * <p>
	 * This method fails if the defined service is not available.
	 * </p>
	 * 
	 * @param clientInterface The Thrift client interface.
	 * @param serviceAddress The address of the service.
	 * @return A client connected to the defined service.
	 */
	public static <T extends TServiceClient> ThriftClient<T, TSocket> createClient(final Class<T> clientInterface, final InetSocketAddress serviceAddress) {
		try {
			final TSocket transport = new TSocket(serviceAddress.getHostName(), serviceAddress.getPort());
			final TProtocol protocol = new TCompactProtocol(transport);
			final Constructor<T> constructor = clientInterface.getConstructor(TProtocol.class);
			final T client = constructor.newInstance(protocol);
			ThriftClient<T, TSocket> thriftClient = new ThriftClient<T, TSocket>(client, transport);
			logger.info("Created {}", thriftClient);
			return thriftClient;
		} catch (final Exception e) {
			logger.error("Error occured while creating Thrift client.",e);
		}
		return null;
	}

	/**
	 * Creates an asynchronous Thrift client.
	 * <p>
	 * The client is initialized with:
	 * <ul>
	 * <li>TNonblockingSocket as transport</li>
	 * <li>TCompactProtocol as protocol</li>
	 * </ul>
	 * </p>
	 * <p>
	 * The returned client can only be used with non-blocking servers.
	 * </p>
	 * 
	 * @param clientInterface The Thrift asynchronous client interface.
	 * @param hostname The hostname of the server.
	 * @param address The address of the service.
	 * @return An asynchronous client connected to the defined service.
	 */
	public static <T extends TAsyncClient> ThriftClient<T, TNonblockingSocket> createAsyncClient(final Class<T> clientInterface,  final InetSocketAddress serviceAddress) {
		try {
			final TNonblockingSocket transport = new TNonblockingSocket(serviceAddress.getHostName(), serviceAddress.getPort());
			final TProtocolFactory protocolFactory = new TCompactProtocol.Factory();
			final Constructor<T> constructor = clientInterface.getConstructor(TProtocolFactory.class,TAsyncClientManager.class,TNonblockingTransport.class);
			final T client = constructor.newInstance(protocolFactory, new TAsyncClientManager(),transport);
			ThriftClient<T, TNonblockingSocket> thriftClient = new ThriftClient<T, TNonblockingSocket>(client, transport);
			logger.info("Created {}", thriftClient);
			return thriftClient;
		} catch (final Exception e) {
			logger.error("Error occured while creating Thrift client.",e);
		}
		return null;
	}


	/**
	 * Creates a {@link TThreadPoolServer}.
	 * <p>
	 * The Server is initialized with:
	 * <ul>
	 * <li>TServerSocket as transport</li>
	 * <li>TCompactProtocol as protocol</li>
	 * <li>The IPv4 address of the local host </li>
	 * <li>An arbitrary free port.</li>
	 * </ul>
	 * <p>
	 * The returned server can be started by calling {@link TServer#serve()}.
	 * </p>
	 * 
	 * @param iface The Thrift service interface.
	 * @param service The service implementation.
	 * @return A client connected to the defined service.
	 * @throws UnknownHostException When if the local host name could not be resolved into an address.
	 * @throws TTransportException When the socket could not be bound.
	 */
	public static ThriftServer<TThreadPoolServer> createThreadPoolServer(Class iface, Object service) throws TTransportException, UnknownHostException  {
		return createThreadPoolServer(iface, service, Inet4Address.getLocalHost(), 0);
	}

	/**
	 * Creates a {@link TThreadPoolServer}.
	 * <p>
	 * The Server is initialized with:
	 * <ul>
	 * <li>TServerSocket as transport</li>
	 * <li>TCompactProtocol as protocol</li>
	 * </ul>
	 * <p>
	 * The returned server can be started by calling {@link ThriftServer#start()}.
	 * </p>
	 * 
	 * @param iface The Thrift service interface.
	 * @param service The service implementation.
	 * @param address The server address.
	 * @param port The server port. If set to 0, the system will pick a free port automatically.
	 * @returns A blocking {@link TThreadPoolServer}.
	 * @throws TTransportException When the socket could not be bound.
	 */
	public static ThriftServer<TThreadPoolServer> createThreadPoolServer(Class iface, Object service, InetAddress address, int port) throws TTransportException {
		InetSocketAddress bindAddr = new InetSocketAddress(address, port);
		final TServerSocket serverTransport = new TServerSocket(bindAddr);
		// when the port parameter is 0, the server socket will have picked a free port 
		if(port == 0)
			bindAddr = (InetSocketAddress) serverTransport.getServerSocket().getLocalSocketAddress();
		TThreadPoolServer.Args args = new TThreadPoolServer.Args(serverTransport);
		args.protocolFactory(new TCompactProtocol.Factory());
		TProcessor processor = createProcessor(iface, service);
		args = args.processor(processor);
		final TThreadPoolServer server = new TThreadPoolServer(args);
		ThriftServer<TThreadPoolServer> thriftServer = new ThriftServer<TThreadPoolServer>(server, iface, bindAddr);
		logger.info("Created {}", thriftServer);
		return thriftServer;
	}


	/**
	 * Creates a non-blocking {@link TThreadedSelectorServer}.
	 * <p>
	 * The Server is initialized with:
	 * <ul>
	 * <li>TNonblockingServerSocket as transport</li>
	 * <li>TCompactProtocol as protocol</li>
	 * </ul>
	 * </p>
	 * <p>
	 * <b>Note:</b><br />
	 * Client have to use the {@link TFramedTransport} in order to connect to a non-blocking server.
	 * </p>
	 * 
	 * @param iface The Thrift service interface.
	 * @param service The service implementation.
	 * @param address The server address.
	 * @param port The server port. If set to 0, the system will pick a free port automatically.
	 * @returns A non-blocking {@link TThreadedSelectorServer}.
	 * @throws TTransportException When the socket could not be bound.
	 */
	public static ThriftServer<TThreadedSelectorServer> createThreadedSelectorServer(@SuppressWarnings("rawtypes") Class iface, Object service, InetAddress address, int port) throws TTransportException  {
			InetSocketAddress bindAddr = new InetSocketAddress(address, port);
			final TNonblockingServerSocket serverTransport = new TNonblockingServerSocket(bindAddr);
			// When the port parameter is 0, the server socket will have picked a free port.
			// Unfortunately, the TNonblockingServerSocket does not provide a getter for it's ServerSocket
			if(port == 0) {
				try {
					Field serverSocketField = TNonblockingServerSocket.class.getDeclaredField("serverSocket_");
					serverSocketField.setAccessible(true);
					ServerSocket serverSocket = (ServerSocket) serverSocketField.get(serverTransport);
					bindAddr = (InetSocketAddress) serverSocket.getLocalSocketAddress();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			TThreadedSelectorServer.Args args = new TThreadedSelectorServer.Args(serverTransport);
			TProcessor processor = createProcessor(iface, service);
			args = args.processor(processor);
			args.protocolFactory(new TCompactProtocol.Factory());
			final TThreadedSelectorServer server = new TThreadedSelectorServer(args);
			ThriftServer<TThreadedSelectorServer> thriftServer = new ThriftServer<TThreadedSelectorServer>(server, iface, bindAddr);
			logger.info("Created {}", thriftServer);
			return thriftServer;
	}

	/**
	 * Creates a non-blocking {@link TThreadedSelectorServer}.
	 * <p>
	 * The Server is initialized with:
	 * <ul>
	 * <li>TNonblockingServerSocket as transport</li>
	 * <li>TCompactProtocol as protocol</li>
	 * </ul>
	 * </p>
	 * <b>Note:</b><br />
	 * Client have to use the {@link TFramedTransport} in order to connect to a non-blocking server.
	 * </p>
	 * 
	 * @param iface The Thrift service interface.
	 * @param service The service implementation.
	 * @param address The address for the service. Set to 0 to let the system pick a free address.
	 * @return A client connected to the defined service.
	 * @throws UnknownHostException When if the local host name could not be resolved into an address.
	 * @throws TTransportException When the socket could not be bound.
	 */
	public static ThriftServer<TThreadedSelectorServer> createThreadedSelectorServer(Class iface, Object service) throws UnknownHostException, TTransportException  {
		return createThreadedSelectorServer(iface, service, Inet4Address.getLocalHost(),0);
	}

	/**
	 * Creates a {@link TProcessor} for the defined Thrift service interface and implementation.
	 * 
	 * @param iface The service interface.
	 * @param service The service implementation.
	 * @return The TProcessor.
	 */
	@SuppressWarnings("unchecked")
	private static TProcessor createProcessor(Class iface, Object service) {
		TProcessor processor = null;
		Class declaringClass = iface.getDeclaringClass();
		if(declaringClass == null)
			throw new IllegalArgumentException(LOG_MSG__INVALID_INTERFACE);
		Class processorClass = null;
		for(Class innerClass: declaringClass.getDeclaredClasses()) {
			if(innerClass.getSimpleName().equals("Processor")) {
				processorClass = innerClass;
				break;
			}
		};

		Constructor procConst;
		try {
			if(processorClass == null)
				throw new IllegalArgumentException(LOG_MSG__INVALID_INTERFACE);
			procConst = processorClass.getConstructor(iface);
			processor = (TProcessor) procConst.newInstance(service);
		} catch (Exception e) {
			logger.error("Error occured while creating TProcessor for Thrift service interface.",e);
		} 

		return processor;
	}

	/**
	 * Disposes the given Thrift Client.
	 * <p>
	 * The client's transports are closed.
	 * </p>
	 * 
	 * @param client
	 */
	public static void disposeClient(TServiceClient client) {
		client.getInputProtocol().getTransport().close();
		client.getOutputProtocol().getTransport().close();
	}
}